<?php

namespace Drupal\Tests\formstack\Kernel;

use Drupal\Core\Site\Settings;
use Drupal\formstack\Formstack;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @group formstack
 */
class FormstackTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['formstack'];

  /** @test */
  public function can_get_access_token_from_settings_file() {
    new Settings(['formstack_access_token' => 'settingsAccessToken123']);

    /** @var Formstack $formstack */
    $formstack = $this->container->get('formstack.formstack');
    $this->assertEquals('settingsAccessToken123', $formstack->getAccessToken());
    $this->assertNotEquals('123abc', $formstack->getAccessToken());
  }

  /** @test */
  public function can_get_access_token_from_config() {
    \Drupal::configFactory()->getEditable('formstack.config')
      ->set('access_token', 'configAccessToken123')
      ->save();

    /** @var Formstack $formstack */
    $formstack = $this->container->get('formstack.formstack');
    $this->assertEquals('configAccessToken123', $formstack->getAccessToken());
    $this->assertNotEquals('123abc', $formstack->getAccessToken());
  }

  /** @test */
  public function access_token_in_settings_file_overrides_config() {
    \Drupal::configFactory()->getEditable('formstack.config')
      ->set('access_token', 'configAccessToken123')
      ->save();
    new Settings(['formstack_access_token' => 'settingsAccessToken123']);

    /** @var Formstack $formstack */
    $formstack = $this->container->get('formstack.formstack');
    $this->assertEquals('settingsAccessToken123', $formstack->getAccessToken());
    $this->assertNotEquals('configAccessToken123', $formstack->getAccessToken());
  }

}
