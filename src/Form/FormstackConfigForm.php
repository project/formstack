<?php

namespace Drupal\formstack\Form;

use Drupal\Core;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\formstack\Formstack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FormstackConfigForm.
 *
 * @package Drupal\formstack\Form
 */
class FormstackConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $config_manager;

  /**
   * @var Formstack
   */
  protected $formstack;

  /**
   *
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ConfigManager $config_manager
  ) {
    parent::__construct($config_factory);
    $this->config_manager = $config_manager;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'formstack.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'formstack_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('formstack.config');
    $form['step_one'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Step 1: Register your application'),
    ];
    $form['step_one']['instructions'] = [
      '#type' => 'markup',
      '#markup' => $this->t($this->formstack_application_instructions_step_one_markup()),
    ];
    $form['step_two'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Step 2: Authorization'),
    ];
    $form['step_two']['instructions'] = [
      '#type' => 'markup',
      '#markup' => $this->t($this->formstack_application_instructions_step_two_markup()),
    ];
    $form['step_two']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('access_token'),
    ];
    $form['step_two']['access_token_instructions'] = [
      '#type' => 'markup',
      '#markup' => $this->t('You can either set the access token here or by adding
         `$settings["formstack_access_token"];` to your settings.php file.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formstack_application_instructions_step_one_markup() {
    global $base_url;
    $site_name = \Drupal::state()->get('site_name');
    $redirect_url = $base_url . $this->current_path();
    $output = <<<FAP
      <h2>You must first register a new application in your Formstack account.</h2>
      <p>Login to your Formstack account and proceed to <a href="https://www.formstack.com/admin/applications/add" target="_blank" >https://www.formstack.com/admin/applications/add</a></p>
      
      <p>In the fields enter the following values:</p>
      <ul>
        <li><strong>Application Name:</strong> $site_name Website</li>
        <li><strong>Redirect URI:</strong> $redirect_url</li>
        <li><strong>Description:</strong> <em>(anything you want)</em></li>
        <li><strong>Logo:</strong> <em>(optional)</em></li>
        <li><strong>Platform:</strong> Website</li>
      </ul>
      <p>Click "Create Application" button</p>
    FAP;
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function formstack_application_instructions_step_two_markup() {
    global $base_url;
    $site_name = \Drupal::state()->get('site_name');
    $redirect_url = $base_url . $this->current_path();
    $output = <<<FAP
      <h2>Enter the your Access Token below from the application you created in Step 1.</h2>
      <p>From the <a href="https://www.formstack.com/admin/apiKey/main" target="_blank" >Applications page on Formstack</a>, click on the application icon. You will find the details in a box on the right side of the window Access Token *</p>
    FAP;
    return $output;
  }

  public function current_path() {
    return \Drupal::service('path.current')->getPath();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if (Settings::get('formstack_access_token') !== NULL) {
      $this->messenger()
        ->addWarning('The access token is currently overridden in the settings file.');
      return;
    }
    $this->config('formstack.config')
      ->set('access_token', $form_state->getValue('access_token'))
      ->save();
  }

}
